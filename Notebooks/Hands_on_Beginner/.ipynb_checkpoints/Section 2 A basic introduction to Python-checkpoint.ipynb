{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# An introduction to Python\n",
    "\n",
    "While it is not possible to give a full introduction to Python in the short time for this course, we aim here to give a brief introduction into some of the main features that are important for the rest of the course. If you have never used Python before we recommend to familiarise yourself before the course using one of the many excellent resources on the net, like the official [Python Tutorial](https://docs.python.org/3/tutorial/)\n",
    "\n",
    "## Learning Outcomes\n",
    "\n",
    "* A basic understanding of the Python programming language\n",
    "* Learn how to install Python using Anaconda\n",
    "* Learn about basic types and their usage\n",
    "* Learn how to write and use functions and classes\n",
    "* What are modules and how to use them\n",
    "* Why you should almost always use Numpy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Why Python\n",
    "\n",
    "### [Trend based on Github users](https://www.developer-tech.com/news/2019/nov/08/octoverse-2019-python-java-github-most-popular-language/)\n",
    "\n",
    "<img src=\"top-programming-languages-2019.png\">\n",
    "\n",
    "### Why did we choose Python for our Labautomation and why should you\n",
    "\n",
    "Python is a free open source programming language with an straight forward syntax. Some of the advantages offered by Python are:\n",
    "\n",
    "* Easy to learn (many educational resources)\n",
    "* Runs on many platforms (from PC to raspberry pi to BBC microbit)\n",
    "* Excellent numerical and scientific tools\n",
    "* General purpose programming language (use it for simulations, lab-automation, scripts, GUIs...)\n",
    "* Easy to read (difficult to write hard to understand code)\n",
    "* It's fun!\n",
    "\n",
    "For lab-automation it is particularly powerful that Python has a straight-forward approach to object-oriented and that it allows easy interfacing with C-code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Further reading\n",
    "\n",
    "* [Python Documentation](https://docs.python.org/3/)\n",
    "* [The Python Tutorial](https://docs.python.org/3/tutorial/)\n",
    "* [The Python Library Reference](https://docs.python.org/3/library/index.html)\n",
    "* [Numpy Quickstart](https://docs.scipy.org/doc/numpy-1.15.0/user/quickstart.html)\n",
    "* [Numpy for Matlab users](https://docs.scipy.org/doc/numpy-1.15.0/user/numpy-for-matlab-users.html)\n",
    "* [Anaconda Docummentation](https://docs.anaconda.com/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to install Python (Everyone starts from here)\n",
    "\n",
    "One of the initially confusing aspects of Python is how to get everything you need installed. \n",
    "\n",
    "Python is separated into the core language and *modules* which can be regarded as the equivalent to libraries in C.\n",
    "The default Python language install contains a large standard library with modules covering everything from regular expressions to logging and webparsing. \n",
    "**However the important modules for numerical/scientific work are not part of the standard library**\n",
    "\n",
    "While it is possible to install all required modules manually it is usually desired to instead install a Python distribution, which has all languages already installed.\n",
    "\n",
    "There are several excellent distribution, arguably the most common and most popular distribution for scientific Python is the [Anaconda](https:///www.anaconda.com) distribution. \n",
    "\n",
    "**If you are using Windows or MAC OS we highly recommend using Anaconda** (Linux users are often also well served by the packages from your package-manager)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Python 2 or Python 3, Python 3!\n",
    "\n",
    "Several years ago Python underwent a incompatible language transistion from version 2 to 3. While both versions are very similar in general, there are some differences that can make code written for one version incompatible with the other. \n",
    "\n",
    "The most current version of Python are 2.7 and 3.7. Because Python 2.7 is unsupported by 2020.\n",
    "\n",
    "**we recommend you use Python 3 and the rest of this course will be based on Python 3**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Installation\n",
    "\n",
    "Installing Anacond is straight forward\n",
    "* Download the installer from [here](https://www.anaconda.com/distribution/#download-section)\n",
    "* Follow the usual installation instructions\n",
    "* Further installation instructions can be found in the [Anaconda Documentation](https://docs.anaconda.com/anaconda/install/)\n",
    "\n",
    "### [Alternative python environment for this class: google colab](https://colab.research.google.com/notebooks/welcome.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Programming Python\n",
    "\n",
    "Writing python programs can be done in two different ways\n",
    "* interactively\n",
    "* non-interactively\n",
    "\n",
    "What to use for programming depends on the user case. For interactive programming you can directly start the python interpreter and begin programming. However, the default interpreter is relatively bare, the two recommended ways for interactive use, which are both installed with Anaconda, are:\n",
    "* The IPython console (a much more feature-full interpreter)\n",
    "* Jupyter notebooks (an interactive environment in the browser, what we are using in this course)\n",
    "\n",
    "If you are writing modules to be included in other programs or scripts to be run non-interactively, you should use an editor. Many editors come with excellent python support, and to cover them all would be beyond this course.\n",
    "\n",
    "### Interactive Development Environments (IDEs)\n",
    "\n",
    "There are several excellent IDEs for Python. Three that we can recomend are:\n",
    "\n",
    "* Spyder (free, open-source, included in Anaconda)\n",
    "* [Pycharm](https://www.jetbrains.com/pycharm/) (Open source community edition and for cost professional edition, free licences for academic use available)\n",
    "* Jupyterlab (next generation jupyternotebooks)\n",
    "* [Atom](https://atom.io/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A first Python program\n",
    "\n",
    "So lets start with the obligatory hello world program"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello World!\n"
     ]
    }
   ],
   "source": [
    "print(\"Hello World!\") ### try keyboard short-cut ('shift-enter')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Python Control flow\n",
    "\n",
    "One of the most controversial features of Python is that it uses white-space as part of it's control flow. Otherwise the constructs are very similar to ones encountered in C or other languages. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### while statement\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0\n",
      "1\n",
      "2\n",
      "3\n",
      "4\n",
      "5\n",
      "6\n",
      "7\n",
      "8\n",
      "9\n"
     ]
    }
   ],
   "source": [
    "i=0\n",
    "while i < 10:\n",
    "    print(i) ### use indention for code blocks\n",
    "    i = i+1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### if then else"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "greater\n"
     ]
    }
   ],
   "source": [
    "x = 1\n",
    "if x > 0:\n",
    "    print(\"greater\")\n",
    "else:\n",
    "    print(\"lesser\")   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "middle\n"
     ]
    }
   ],
   "source": [
    "x = 3\n",
    "if x < 0:\n",
    "    print(\"lesser\")\n",
    "elif x > 0 and x < 4:\n",
    "    print(\"middle\")\n",
    "else:\n",
    "    print(\"high\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### for loops \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1\n",
      "2\n",
      "3\n"
     ]
    }
   ],
   "source": [
    "for i in [1,2,3]:\n",
    "    print(i)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When iterating over a range of numbers it's typically easiest to use the inbuild `range()` function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0\n",
      "1\n",
      "2\n"
     ]
    }
   ],
   "source": [
    "for i in range(3):\n",
    "    print(i)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The continue, break and else statements on a for loop\n",
    "\n",
    "Three statements that can be associated with a loop are `continue`, `break` and (somewhat unintuitively) `else`\n",
    "\n",
    "* The `break` statement breaks out of (exits) a running loop before the end condition is met"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0\n",
      "1\n",
      "2\n",
      "3\n",
      "4\n",
      "i when breaking = 5\n"
     ]
    }
   ],
   "source": [
    "for i in range(10):\n",
    "    if i > 4:\n",
    "        break\n",
    "    print(i)\n",
    "print(\"i when breaking = {}\".format(i))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The `continue` statement continues with the next iteration of the loop, without executing the rest of the loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "4\n",
      "5\n",
      "6\n",
      "7\n",
      "8\n",
      "9\n"
     ]
    }
   ],
   "source": [
    "for i in range(10):\n",
    "    if i < 4:\n",
    "        continue\n",
    "    print(i)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* The `else` statement for `for` loops is relatively unknown. It executes when the loop has exited (ended) without encountering a break statement. This can be very useful for searches, see e.g. the following example from the official documentation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2 is a prime number\n",
      "3 is a prime number\n",
      "4 equals 2 * 2.0\n",
      "5 is a prime number\n",
      "6 equals 2 * 3.0\n",
      "7 is a prime number\n",
      "8 equals 2 * 4.0\n",
      "9 equals 3 * 3.0\n"
     ]
    }
   ],
   "source": [
    "for n in range(2, 10):\n",
    "    for x in range(2, n):\n",
    "        if n % x == 0:\n",
    "            print( n, 'equals', x, '*', n/x)\n",
    "            break\n",
    "    else:\n",
    "        # loop fell through without finding a factor\n",
    "        print(n, 'is a prime number')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Python functions\n",
    "\n",
    "Python functions are defined using the `def` statement. Everything to be executed in the function is indented"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fct(x):\n",
    "    print(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3\n"
     ]
    }
   ],
   "source": [
    "fct(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To return values from a function we use the `return` statement, note it is possible to return more than one value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fct(x):\n",
    "    y = x+3\n",
    "    return y, 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(6, 3)"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "fct(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Multiple function arguments are seperated by commas. It is also possible to define arguments with default values (keyword arguments). All non-default arguments must come before default ones. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fct2(x, y, z=3):\n",
    "    return x*y + z"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1*2+10= 5\n",
      "1*2+3= 5\n",
      "1*2+5= 7\n"
     ]
    }
   ],
   "source": [
    "print(\"1*2+10=\",fct2(1,2))\n",
    "print(\"1*2+3=\",fct2(1,2,3))\n",
    "print(\"1*2+5=\",fct2(1,2, z=5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Python classes\n",
    "\n",
    "While Python is a mult-paradigm programming language, object-oriented programming (OOP) always had a strong influence on its design. \n",
    "\n",
    "OOP is one of the most popular approaches solving problems. In a nutshell it is about structuring your code into objects as the key component, where an object is defined by two characteristics:\n",
    "* attributes\n",
    "* behaviour \n",
    "\n",
    "For example a rectangle is defined through the length of its two sides\n",
    "\n",
    "Classes are central to OOP as they provide the blueprint for objects. In Python a Class is created with the `class` statement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Rectangle: # by convention classes are typically spelled in Camelcase\n",
    "    x = 1.\n",
    "    y = 2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class '__main__.Rectangle'>\n"
     ]
    }
   ],
   "source": [
    "p = Rectangle()\n",
    "print(type(p))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Class attributes are accessed through by using a '.' after the instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.0\n",
      "1.0\n"
     ]
    }
   ],
   "source": [
    "print(p.x)\n",
    "p.y = 1.\n",
    "print(p.y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also associate behaviour with objects by defining functions on the object, so-called *methods*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2.0"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "class Rectangle:\n",
    "    x = 1.\n",
    "    y = 2.\n",
    "    def area(self): # note that self (a reference to the object itself) is always the first argument \n",
    "        return self.x*self.y\n",
    "r = Rectangle()\n",
    "r.area()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## An overview of Python data types\n",
    "\n",
    "Let us have a brief overview (but incomplete) overview of some of the python types you will most commonly encounter. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Strings\n",
    "\n",
    "Because Python is a dynamic language, you do not have to declare variables and they can also change types.\n",
    "\n",
    "Strings are how text is represented in Python. You indicate a string through single (') or double (\") quotes. \n",
    "\n",
    "*Note*: that since Python 3 strings are actually unicode entities, they therefore a somewhat more abstract concept and are not what is e.g. stored directly on disk. If you want to use actual byte representations you should use the *byte* type. In most cases one encounters in instrument automation, this does not matter however.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets create a string"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = \"Hello World\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello World\n"
     ]
    }
   ],
   "source": [
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can do easy operations on strings such as splitting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [],
   "source": [
    "greet, name = a.split(\" \")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and concatenating"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello Nick\n"
     ]
    }
   ],
   "source": [
    "new_hello = greet + \" Nick\"\n",
    "print(new_hello)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic numeric types\n",
    "\n",
    "Similar to most languages Python supports the typical numeric data types:\n",
    "* integers\n",
    "* floats\n",
    "* complex numbers\n",
    "\n",
    "Unlike C there is however no difference between e.g. `float` and `double` representations (not true for numpy). Also there is a `long` integer type, which has unlimited precision. And a `bool` type which is essentially a subset of the integer type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 2\n",
    "y = 2.\n",
    "z = 2. + 2.*1j"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'int'>\n",
      "<class 'float'>\n",
      "<class 'complex'>\n"
     ]
    }
   ],
   "source": [
    "print(type(x))\n",
    "print(type(y))\n",
    "print(type(z))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python (similar to Matlab) supports mixed type artithmetic. Thus for in operation between two numerical types, the 'narrower' type will be converted to a higher type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "4.0\n",
      "<class 'float'>\n"
     ]
    }
   ],
   "source": [
    "xx = x + y\n",
    "print(xx)\n",
    "print(type(xx))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(4+2j)\n",
      "<class 'complex'>\n"
     ]
    }
   ],
   "source": [
    "yy = z + y\n",
    "print(yy)\n",
    "print(type(yy))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All typical operators are supported"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.0"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x - y # subtraction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4.0"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x + y # addition"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4.0"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x * y # multiplication"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1.0"
      ]
     },
     "execution_count": 32,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x / y # division"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.5"
      ]
     },
     "execution_count": 33,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "4.5 % y # remainder"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2.0"
      ]
     },
     "execution_count": 34,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "4.5 // y # floor division"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4"
      ]
     },
     "execution_count": 35,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x ** 2 # power"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Caution**: The meaning of the `/` operator changed for integers between Python 2 and 3. `2/3` was an integer division. Now it generates a float."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.6666666666666666"
      ]
     },
     "execution_count": 36,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "2/3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You need to explicitely ask for integer division `//`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 37,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "2//3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Lists and Dictionaries\n",
    "\n",
    "Two other convenient types are Lists and Dictionaries. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Lists\n",
    "\n",
    "A list is essentially a ordered container of types and objects indicated by `[`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 2, 3, 4]\n",
      "1\n",
      "4\n"
     ]
    }
   ],
   "source": [
    "x = [1, 2, 3, 4]\n",
    "print(x)\n",
    "print(x[0])\n",
    "print(x[3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Note* that Python indices start at 0!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Importantly the different elements do not have to be the same type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "metadata": {},
   "outputs": [],
   "source": [
    "x2 = [1, 2. , 2+2j, [2, 'a', 'b']]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 2.0, (2+2j), [2, 'a', 'b']]\n",
      "(2+2j)\n",
      "[2, 'a', 'b']\n"
     ]
    }
   ],
   "source": [
    "print(x2)\n",
    "print(x2[2])\n",
    "print(x2[3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Tuples\n",
    "\n",
    "Tuples are a similar structure to lists, providing an ordered container."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Dictionaries\n",
    "\n",
    "A dictionary is an mapping type (similar to a hash) and are cast with the `{`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "metadata": {},
   "outputs": [],
   "source": [
    "c = {'a': 1, \"b\": 2., 4: 5}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'a': 1, 'b': 2.0, 4: 5}\n",
      "<class 'dict'>\n"
     ]
    }
   ],
   "source": [
    "print(c)\n",
    "print(type(c))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1\n"
     ]
    }
   ],
   "source": [
    "print(c['a'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "5\n"
     ]
    }
   ],
   "source": [
    "print(c[4])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2.0\n"
     ]
    }
   ],
   "source": [
    "print(c['b'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Note*: Dictionaries are unordered. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Numpy arrays\n",
    "\n",
    "While initially tempting using lists to present arrays is generally not a good idea."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]\n",
      "[2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0]\n"
     ]
    }
   ],
   "source": [
    "x = [1.]*100\n",
    "y = [2.]*100\n",
    "print(x)\n",
    "print(y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "metadata": {},
   "outputs": [
    {
     "ename": "TypeError",
     "evalue": "can't multiply sequence by non-int of type 'list'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-47-3d9d541540c2>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mz\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mx\u001b[0m\u001b[0;34m*\u001b[0m\u001b[0my\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m: can't multiply sequence by non-int of type 'list'"
     ]
    }
   ],
   "source": [
    "z=x*y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0]\n"
     ]
    }
   ],
   "source": [
    "z=[]\n",
    "for i in range(len(x)):\n",
    "    z.append(x[i]*y[i])\n",
    "print(z)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "metadata": {},
   "outputs": [],
   "source": [
    "def listmult(x,y):\n",
    "    z = []\n",
    "    for i in range(len(x)):\n",
    "        z.append(x[i]*y[i])\n",
    "    return z"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 50,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "12.4 µs ± 392 ns per loop (mean ± std. dev. of 7 runs, 100000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%timeit listmult(x,y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is very slow. To overcome the lack of an array type the *Numpy* module was born.\n",
    "\n",
    "### Using modules"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Modules are similar to libraries in C. They can contain new classes, types and functions and in general are simply a Python file (or a set of files). To use a module we have to import it"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 51,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Note* unlike e.g. Matlab imported modules live in their own namespace. Thus to access a function from `numpy` I have to explicitely specify it"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1.0"
      ]
     },
     "execution_count": 52,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "numpy.sin(numpy.pi/2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are several convenient way around naming imports and selective imports"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1.0"
      ]
     },
     "execution_count": 53,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import numpy as np # imports numpy and renames it to 'np', this is the defacto standard way to import numpy\n",
    "np.sin(np.pi/2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1.0"
      ]
     },
     "execution_count": 54,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from numpy import sin\n",
    "sin(np.pi/2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1.0"
      ]
     },
     "execution_count": 55,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from numpy import sin as nsin\n",
    "nsin(np.pi/2)\n",
    "# from numpy import * # is also possible but generally frowned upon because you could overwrite functions in your namespace"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises (10 mins)\n",
    "\n",
    "* Create a dictionary\n",
    "* Iterate over the keys and values and print them\n",
    "* Create a list\n",
    "* Add an item to the list\n",
    "* Remove an item from the list\n",
    "* Combine the strings \"hello\" and \"world\"\n",
    "* Try what happens when you modify a tuple\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
