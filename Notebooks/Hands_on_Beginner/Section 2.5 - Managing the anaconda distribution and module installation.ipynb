{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Managing anaconda and installing modules\n",
    "\n",
    "As we alluded to in the previous section adding functionality to Python is done through modules and packages. If you are using the _anaconda_ distribution most of the central numerical packages will be installed already, however occasionally you likely will require to add non-default module or package. In this section we will introduce how to install extra packages and how to keep your installation up to date. \n",
    "\n",
    "## Learning outcomes\n",
    "\n",
    "In this section you will learn \n",
    "\n",
    "* about the different ways to install modules in Python\n",
    "* how to find and install modules using the _anaconda_ GUI and command line\n",
    "* how to find and install modules with `pip` the python package manager \n",
    "* how to keep your installed packages up to date\n",
    "\n",
    "## References and further reading\n",
    "\n",
    "* [Real Python modules and packages](https://realpython.com/python-modules-packages/)\n",
    "* [Python tutorial on modules](https://docs.python.org/3/tutorial/modules.html)\n",
    "* [Python import and the search path](https://www.devdungeon.com/content/python-import-syspath-and-pythonpath-tutorial)\n",
    "* [Conda user guide](https://conda.io/projects/conda/en/latest/user-guide/index.html)\n",
    "* [Installing packages tutorial](https://packaging.python.org/tutorials/installing-packages/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Modules vs packages\n",
    "\n",
    "Before we dive into the details of let us briefly discuss the difference between a module and a package. \n",
    "\n",
    "### Module\n",
    "\n",
    "A module is a file containing Python functions and statements, which is added with the `import` statement. If you create a file `fibonacci.py` in this directory with the contents \n",
    "```python\n",
    "c = 2.99792458e8\n",
    "\n",
    "def fib(n):    # write Fibonacci series up to n\n",
    "    a, b = 0, 1\n",
    "    while a < n:\n",
    "        print(a, end=' ')\n",
    "        a, b = b, a+b\n",
    "    print()\n",
    "```\n",
    "\n",
    "We can import and run the functions or access the constants in this file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0 1 1 2 3 \n",
      "The speed of light in vacuum is= 299792458.0 m/s\n"
     ]
    }
   ],
   "source": [
    "import fibonacci\n",
    "\n",
    "fibonacci.fib(5)\n",
    "print(\"The speed of light in vacuum is= {} m/s\".format(fibonacci.c))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Package\n",
    "\n",
    "A package on the other hand is a collection of modules under a single namespace. Generally most external installations are packages, examples are `numpy`, `matplotlib`, `scipy`. Modules within a package are separated with dot (and we can go many layers deep). For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<module 'numpy.polynomial.chebyshev' from '/usr/lib64/python3.7/site-packages/numpy/polynomial/chebyshev.py'>"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import numpy as np\n",
    "\n",
    "np.polynomial.chebyshev"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<function numpy.polynomial.chebyshev.chebadd(c1, c2)>"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.polynomial.chebyshev.chebadd"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Package installation\n",
    "\n",
    "Generally speaking Python can find modules to import in a number of locations (see https://www.devdungeon.com/content/python-import-syspath-and-pythonpath-tutorial) and it is therefore possible to install packages using many different ways, however it is generally good to use a some package manager. The most common ways to install external packages are:\n",
    "\n",
    "* Through the distribution (_anaconda_)\n",
    "    * using the _Anaconda Navigator_\n",
    "    * using the `conda` command\n",
    "* Using the `pip` the Python package manager\n",
    "* From source (not covered here)\n",
    "* Using the operating system package manager (mainly Linux, also not covered here)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Installation using Anaconda\n",
    "\n",
    "We recommend beginners to use the _anaconda_ distribution, because most of the important packages are already distributed. Moreover, the `conda` package manager makes installing and updating packages easy, in particular for cases where packages depend on non-python libraries, which does occassionally cause difficulties for `pip`. \n",
    "\n",
    "There are essentially two ways of installing packages in _anaconda_\n",
    "1. Using the _Anaconda Navigator_\n",
    "2. Using the `conda` commandline utility\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Installation using the GUI\n",
    "\n",
    "In Windows, search for _anaconda-navigator_ in the start menu , whereas on Linux, the GUI can be started from the command line using the command `anaconda-navigator`.\n",
    "The GUI can be used to create new python **environments**, which is the prefered way to modify and update from the base _root_ environment present after running the anaconda installer (see [website](https://docs.anaconda.com/anaconda/navigator/getting-started/#navigator-starting-navigator) for more detailed instructions).\n",
    "Once a new environment is created, _anaconda-navigator_ can be used to search for packages and install them.\n",
    "Once the newly created environment is completed by installing all the desired packages, the GUI home tab can be used to start various Python sessions including:\n",
    "\n",
    "- Spyder\n",
    "- JupyterLab\n",
    "- Jupyter Notebook\n",
    "- Qt Console\n",
    "\n",
    "The GUI also provides useful link to various Python documentation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Installation using `conda`\n",
    "\n",
    "[conda](https://docs.conda.io/projects/conda/en/latest/commands.html#conda-general-commands) is the preferred command-line tool to install and manage packages and environments in the Anaconda Python distribution.\n",
    "Below a few useful _conda_ commands:\n",
    "\n",
    "\n",
    "#### Install a package\n",
    "\n",
    "`conda install PACKAGE_NAME`\n",
    "\n",
    "\n",
    "#### Update a package\n",
    "\n",
    "`conda update PACKAGE_NAME`\n",
    "\n",
    "\n",
    "#### Uninstall a package\n",
    "\n",
    "`conda remove PACKAGE_NAME`\n",
    "\n",
    "\n",
    "#### Create an environment\n",
    "\n",
    "`conda create --name ENVIRONMENT_NAME python`\n",
    "\n",
    "\n",
    "\n",
    "#### Activate an environment\n",
    "\n",
    "`conda activate ENVIRONMENT_NAME`\n",
    "\n",
    "\n",
    "#### Deactivate an environment\n",
    "\n",
    "`conda deactivate`\n",
    "\n",
    "\n",
    "#### Search available packages\n",
    "\n",
    "`conda search SEARCH_TERM`\n",
    "\n",
    "\n",
    "### Install package from specific source like for example from [conda-forge](https://conda-forge.org/)\n",
    "\n",
    "`conda install -c conda-forge PACKAGE_NAME`\n",
    "\n",
    "\n",
    "#### List installed packages\n",
    "\n",
    "`conda list --name ENVIRONMENT_NAME`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Keeping anaconda up to date\n",
    "\n",
    "One of the advantages of using _anaconda_ is that it is relatively straight forward to keep packages up to date. This is based achieved using `conda`. You can find a detailed discussion of updating _anaconda_ on their [website](https://www.anaconda.com/keeping-anaconda-date/). In short if you want to update a single package simply use `conda update [pkg name]`. If you want to update the full _anaconda_ distribution use `conda update --all`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Package installation with `pip`\n",
    "\n",
    "Sometimes packages can not be found in the conda repositories, however they can often be found in at [PyPI](https://pypi.org) the python package index. The tool for searching and installing packages from PyPI is `pip` the python package manager. \n",
    "\n",
    "`pip` is a commandline tool for searching, installing removing and upgrading packages.\n",
    "\n",
    "### Searching for packages\n",
    "\n",
    "The way to find to find packages on PyPI used to be the search command `pip search [pkgname]`, however unfortunately the [PyPI](https://pypi.org) package index had been hit by too many request from a large network of servers that made maintaining the tool unsustainable.  \n",
    "\n",
    "There are currently two solutions:\n",
    "\n",
    "1. Search on the website [https://pypi.org](https://pypi.org) \n",
    "2. Install `pip-search` and use the `pip_search` commandline tool (note the `-` in the packagename, but `_` in the command)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-----------------  -------------------------------------------------------------------------------------------------------------------------------------------------------\r\n",
      "Name               Description\r\n",
      "\r\n",
      "numpy              NumPy is the fundamental package for array computing with Python.\r\n",
      "numpy1             Python\r\n",
      "numpy-cloud        Numpy in the cloud\r\n",
      "numpy-sugar        Missing NumPy functionalities\r\n",
      "numpy-financial    Simple financial functions\r\n",
      "BSON-NumPy         Module for converting directly from BSON to NumPy ndarrays and vice versa\r\n",
      "topas2numpy        Python functions for reading TOPAS result files\r\n",
      "numpy-serializer   Preserve numpy arrays shapes while serializing them to bytes\r\n",
      "mapchete-numpy     Mapchete NumPy read/write extension\r\n",
      "hypothesis-numpy2  Provides strategies for generating various `numpy` objects\r\n",
      "gdal2numpy         A utils functions package\r\n",
      "numpy-camera       ???\r\n",
      "numpy_ringbuffer   Ring buffer implementation for numpy\r\n",
      "numpy-turtle       Turtle graphics with NumPy\r\n",
      "numpy-linreg       Linear Regression with numpy only.\r\n",
      "numpy-partition    SQL PARTITION BY and window functions for NumPy\r\n",
      "preconvert_numpy   Preconverts common numpy types to their serializable (jsonifiable, msgpackable, etc...) form.\r\n",
      "numpy-nn           Numpy NN is a Deep Neural Network Package which is built on base Numpy operations. This project is under development and any contributions are welcome.\r\n",
      "-----------------  -------------------------------------------------------------------------------------------------------------------------------------------------------\r\n"
     ]
    }
   ],
   "source": [
    "!pip_search numpy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Installing packages from PyPI\n",
    "\n",
    "To install the package use the `pip install [pkgname]` command. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Installing from github\n",
    "\n",
    "Sometimes a package might not be on PyPI but can be found on e.g. github. You can use the `pip install -e <url>` switch that installs from a given url. So for example to install _bokeh_ from git use `pip install -e git+https://github.com/bokeh/bokeh`. Note this also works for different version control systems (the `git+` part in the url) or other websites like e.g. gitlab.com"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Removing and upgrading\n",
    "\n",
    "#### Removing\n",
    "Removing packages is also straight forward using `pip uninstall [pkgname]`. \n",
    "\n",
    "#### Upgrading\n",
    "To upgrade a package use `pip install --upgrade [pkgname]`. There is no command to upgrade all packages in one go, however it is possible to get a list of upgradable packages which is `pip list --outdated`\n",
    "\n",
    "### `pip help`\n",
    "\n",
    "`pip` offers an extensive help system which can be accessed with `pip help` and `pip help [command]`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A note on virtual environments\n",
    "\n",
    "There is often advise on using virtual environments. This can be often advantageous if one needs different package versions for different projects or to generally manage your environments and do not want to install all packages glo. A discussion of the different ways to create and use virtual environements can be found [here](https://medium.com/@krishnaregmi/pipenv-vs-virtualenv-vs-conda-environment-3dde3f6869ed) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises\n",
    "\n",
    "* Use `pip` or `conda` to install the `VISA` bindings for Python (note that unfortunately there are also bindings to the VISA card API, so check you are installing the correct module)\n",
    "* Install `pyvisa-sim` and  `pyvisa-py` a simulated backend and a python backend for the Python `VISA` bindings\n",
    "* Find all the installed packages via `pip`\n",
    "* What does the `-e` option for `pip install` do. How could it be useful for working on packages?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
