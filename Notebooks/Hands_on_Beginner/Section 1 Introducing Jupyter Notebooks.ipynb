{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# An Introduction to Jupyter Notebooks\n",
    "\n",
    "In this section you will learn about Jupyter Notebooks and how to use them for interactive programming with Python.\n",
    "\n",
    "The labautomation course is written using jupyter notebooks. \n",
    "\n",
    "## Learning Outcomes\n",
    "\n",
    "You will learn how to:\n",
    "* Install Jupyter\n",
    "* Start a notebook server\n",
    "* Program in notebooks\n",
    "* Write mardown text\n",
    "\n",
    "## References\n",
    "\n",
    "* https://jupyter.org/\n",
    "* [LIGO tutorial notebooks](https://www.gw-openscience.org/tutorials/)\n",
    "* [binder online interactive notebooks](https://mybinder.org/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What is Jupyter\n",
    "\n",
    "From the Jupyter project website:\n",
    ">The Jupyter Notebook is an open-source web application that allows you to create and share documents that contain live code, equations, visualizations and narrative text.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Installing\n",
    "\n",
    "There are different ways of installing jupyter notebooks. For beginners we recommend using the *anaconda* distribution, which installs. If you are more experienced you can also install jupyter using `pip` or if you are on Linux using your package manager."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running\n",
    "\n",
    "Jupyter notebooks are accessed via the browser, however in order to do this you need to first start a server. You can start the server either from the *anaconda* start screen, or from an anaconda console. Note that when starting from the console the directory where you start the server is going to be the \"home\" directory where your notebooks get created. \n",
    "\n",
    "You can also open a command line window (Windows) or terminal (Mac OS or Linux), and type 'jupyter notebook' to start a server."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interface\n",
    "\n",
    "When you first start the server you will see the *dashboard*, which should look something like this:\n",
    "\n",
    "![dashboard](jupyter-dashboard.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is the dashboard from where you can create new notebooks, or open existing notebooks. \n",
    "\n",
    "Once you have started a notebook you can see the toolbar and the editor area. The first thing you should do is to check the user interface help."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cell modes\n",
    "\n",
    "Jupyter notebooks have two different modes\n",
    "\n",
    "**Edit mode** where you can edit cells and enter or text into cells\n",
    "\n",
    "**Command mode** where you execute cells, move them up and down etc.\n",
    "\n",
    "The two modes are indicated by the border and the bar on the right. In *Edit mode* the bar is green, in *Command mode* the bar is blue."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Executing cells\n",
    "\n",
    "The cells are essentially small execution environments. You enter your code and then execute using `shift-Enter`. The output will be printed below the cell"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "hello world\n"
     ]
    }
   ],
   "source": [
    "print(\"hello world\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Execution order\n",
    "\n",
    "Have you noticed the numbers in front of the cells? They indicate the execution order. \n",
    "\n",
    "**Importantly** the execution order is not necessarily the order of the cells, because you can always go back and forth cand excute different cells with a random order."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic Shortcuts\n",
    "\n",
    "To more easily work with notebooks you should know some of the keyboard short-cuts. You can look them up under the help menu. The shortcuts are different in the two modes and differ slightly by operating system. \n",
    "\n",
    "The most important ones are:\n",
    "\n",
    "**Command mode**\n",
    "\n",
    "* `shift + enter` run cell, select below\n",
    "* `ctrl + enter` run cell\n",
    "* `alt + enter` run cell, insert below\n",
    "* `A` insert cell above\n",
    "* `B` insert cell below\n",
    "* `C` copy cell\n",
    "* `V` paste cell\n",
    "* `D`, `D` delete selected cell\n",
    "* `shift + M` merge selected cells, or current cell with cell below if only one cell selected\n",
    "* `I`, `I` interrupt kernel\n",
    "* `0` , `0` restart kernel (with dialog)\n",
    "* `Y` change cell to code mode\n",
    "* `M` change cell to markdown mode (good for documentation)\n",
    "\n",
    "\n",
    "**Edit mode**\n",
    "\n",
    "* `ctrl + /` toggle comment lines\n",
    "* `tab` code completion or indent\n",
    "* `shift + tab` documentation for the command under the cursor.\n",
    "* `ctrl + shift + -` split cell\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Text\n",
    "\n",
    "One of the great features about Jupyter notebooks is that you can add structured text to your notebooks. You can thus use them for more than just coding, but also document your code, or even write a paper using the notebooks.\n",
    "\n",
    "### Markdown\n",
    "\n",
    "The way to write structured text in notebooks is using the `markdown` dialect. Markdown is a lightweight, easy to learn markup language for formatting plain text. Its syntax has a one-to-one correspondance with HTML tags, so some prior knowledge here would be helpful but is definitely not a prerequisite. It is a simple way of adding to structure to your documents that remains readable in text. \n",
    "\n",
    "```\n",
    "# This is a level 1 heading\n",
    "## This is a level 2 heading\n",
    "This is some plain text that forms a paragraph.\n",
    "Add emphasis via **bold** and __bold__, or *italic* and _italic_.\n",
    "\n",
    "Paragraphs must be separated by an empty line.\n",
    "\n",
    "* Sometimes we want to include lists.\n",
    " * Which can be indented.\n",
    "\n",
    "1. Lists can also be numbered.\n",
    "2. For ordered lists.\n",
    "\n",
    "[It is possible to include hyperlinks](https://www.example.com)\n",
    "\n",
    "Inline code uses single backticks: `foo()`, and code blocks use triple backticks:\n",
    "\n",
    "```\n",
    "bar()\n",
    "```\n",
    "\n",
    "Or can be intented by 4 spaces:\n",
    "\n",
    "    foo()\n",
    "\n",
    "And finally, adding images is easy: ![Alt text](https://www.example.com/image.jpg)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Magic Commands\n",
    "\n",
    "Jupyter includes also a number of 'magic' commands that enable accessing special functions. There are two type of magic commands:\n",
    "\n",
    "1. commands indicated with a `%` sign are line magic commands everything on the same line is part of the magic command\n",
    "2. commands with `%%` are cell magic commands everything in the cell is a magic command\n",
    "\n",
    "Jupyter has a magic command to show all magic commands"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/json": {
       "cell": {
        "!": "OSMagics",
        "HTML": "Other",
        "SVG": "Other",
        "bash": "Other",
        "capture": "ExecutionMagics",
        "debug": "ExecutionMagics",
        "file": "Other",
        "html": "DisplayMagics",
        "javascript": "DisplayMagics",
        "js": "DisplayMagics",
        "latex": "DisplayMagics",
        "markdown": "DisplayMagics",
        "perl": "Other",
        "prun": "ExecutionMagics",
        "pypy": "Other",
        "python": "Other",
        "python2": "Other",
        "python3": "Other",
        "ruby": "Other",
        "script": "ScriptMagics",
        "sh": "Other",
        "svg": "DisplayMagics",
        "sx": "OSMagics",
        "system": "OSMagics",
        "time": "ExecutionMagics",
        "timeit": "ExecutionMagics",
        "writefile": "OSMagics"
       },
       "line": {
        "alias": "OSMagics",
        "alias_magic": "BasicMagics",
        "autoawait": "AsyncMagics",
        "autocall": "AutoMagics",
        "automagic": "AutoMagics",
        "autosave": "KernelMagics",
        "bookmark": "OSMagics",
        "cat": "Other",
        "cd": "OSMagics",
        "clear": "KernelMagics",
        "colors": "BasicMagics",
        "conda": "PackagingMagics",
        "config": "ConfigMagics",
        "connect_info": "KernelMagics",
        "cp": "Other",
        "debug": "ExecutionMagics",
        "dhist": "OSMagics",
        "dirs": "OSMagics",
        "doctest_mode": "BasicMagics",
        "ed": "Other",
        "edit": "KernelMagics",
        "env": "OSMagics",
        "gui": "BasicMagics",
        "hist": "Other",
        "history": "HistoryMagics",
        "killbgscripts": "ScriptMagics",
        "ldir": "Other",
        "less": "KernelMagics",
        "lf": "Other",
        "lk": "Other",
        "ll": "Other",
        "load": "CodeMagics",
        "load_ext": "ExtensionMagics",
        "loadpy": "CodeMagics",
        "logoff": "LoggingMagics",
        "logon": "LoggingMagics",
        "logstart": "LoggingMagics",
        "logstate": "LoggingMagics",
        "logstop": "LoggingMagics",
        "ls": "Other",
        "lsmagic": "BasicMagics",
        "lx": "Other",
        "macro": "ExecutionMagics",
        "magic": "BasicMagics",
        "man": "KernelMagics",
        "matplotlib": "PylabMagics",
        "mkdir": "Other",
        "more": "KernelMagics",
        "mv": "Other",
        "notebook": "BasicMagics",
        "page": "BasicMagics",
        "pastebin": "CodeMagics",
        "pdb": "ExecutionMagics",
        "pdef": "NamespaceMagics",
        "pdoc": "NamespaceMagics",
        "pfile": "NamespaceMagics",
        "pinfo": "NamespaceMagics",
        "pinfo2": "NamespaceMagics",
        "pip": "PackagingMagics",
        "popd": "OSMagics",
        "pprint": "BasicMagics",
        "precision": "BasicMagics",
        "prun": "ExecutionMagics",
        "psearch": "NamespaceMagics",
        "psource": "NamespaceMagics",
        "pushd": "OSMagics",
        "pwd": "OSMagics",
        "pycat": "OSMagics",
        "pylab": "PylabMagics",
        "qtconsole": "KernelMagics",
        "quickref": "BasicMagics",
        "recall": "HistoryMagics",
        "rehashx": "OSMagics",
        "reload_ext": "ExtensionMagics",
        "rep": "Other",
        "rerun": "HistoryMagics",
        "reset": "NamespaceMagics",
        "reset_selective": "NamespaceMagics",
        "rm": "Other",
        "rmdir": "Other",
        "run": "ExecutionMagics",
        "save": "CodeMagics",
        "sc": "OSMagics",
        "set_env": "OSMagics",
        "store": "StoreMagics",
        "sx": "OSMagics",
        "system": "OSMagics",
        "tb": "ExecutionMagics",
        "time": "ExecutionMagics",
        "timeit": "ExecutionMagics",
        "unalias": "OSMagics",
        "unload_ext": "ExtensionMagics",
        "who": "NamespaceMagics",
        "who_ls": "NamespaceMagics",
        "whos": "NamespaceMagics",
        "xdel": "NamespaceMagics",
        "xmode": "BasicMagics"
       }
      },
      "text/plain": [
       "Available line magics:\n",
       "%alias  %alias_magic  %autoawait  %autocall  %automagic  %autosave  %bookmark  %cat  %cd  %clear  %colors  %conda  %config  %connect_info  %cp  %debug  %dhist  %dirs  %doctest_mode  %ed  %edit  %env  %gui  %hist  %history  %killbgscripts  %ldir  %less  %lf  %lk  %ll  %load  %load_ext  %loadpy  %logoff  %logon  %logstart  %logstate  %logstop  %ls  %lsmagic  %lx  %macro  %magic  %man  %matplotlib  %mkdir  %more  %mv  %notebook  %page  %pastebin  %pdb  %pdef  %pdoc  %pfile  %pinfo  %pinfo2  %pip  %popd  %pprint  %precision  %prun  %psearch  %psource  %pushd  %pwd  %pycat  %pylab  %qtconsole  %quickref  %recall  %rehashx  %reload_ext  %rep  %rerun  %reset  %reset_selective  %rm  %rmdir  %run  %save  %sc  %set_env  %store  %sx  %system  %tb  %time  %timeit  %unalias  %unload_ext  %who  %who_ls  %whos  %xdel  %xmode\n",
       "\n",
       "Available cell magics:\n",
       "%%!  %%HTML  %%SVG  %%bash  %%capture  %%debug  %%file  %%html  %%javascript  %%js  %%latex  %%markdown  %%perl  %%prun  %%pypy  %%python  %%python2  %%python3  %%ruby  %%script  %%sh  %%svg  %%sx  %%system  %%time  %%timeit  %%writefile\n",
       "\n",
       "Automagic is ON, % prefix IS NOT needed for line magics."
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%lsmagic"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises (5-10 mins)\n",
    "\n",
    "* Start a notebook server\n",
    "* Do a hello world program\n",
    "* Find out what the %run magic command does (hint use the `shift-tab` documentation)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
